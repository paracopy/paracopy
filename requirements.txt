babel==2.15.0
distro==1.9.0
flet==0.22
pathvalidate==3
pydantic==2.7.1
pyperclip==1.8.2
pyudev==0.24.1
aiohttp==3.9.5

build==1.2.1
setuptools==70.0.0
setuptools_scm==8.1.0