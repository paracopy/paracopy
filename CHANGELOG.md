# 1.2.0 (2024-06-29)


### Bug Fixes

* Add max version for dependencies ([fa5f992](https://gitlab.com/paracopy/paracopy/commit/fa5f992ebe16f024ae949e4e07ce09edad6b511e))


### Features

* Create or update desktop entry for ParaCopy ([6cbb262](https://gitlab.com/paracopy/paracopy/commit/6cbb262ca3b8ef63378ffe71478d65dd78c6c8be))



# 1.1.0 (2024-06-25)


### Bug Fixes

* Correct missing number formatting for source list ([5c210c5](https://gitlab.com/paracopy/paracopy/commit/5c210c5dcd22a343f6699a7f92d1c7176bd97206))


### Features

* Installation Check ([318d138](https://gitlab.com/paracopy/paracopy/commit/318d138f715e566d684c062e15fee293ff14d3a2))
* Translate ParaCopy to english ([bdf01e4](https://gitlab.com/paracopy/paracopy/commit/bdf01e47a64e5cd1b3c43a570d07818f1bab11b7))



## v1.0.0 (2024-06-08)

* Initial version with only very basic features